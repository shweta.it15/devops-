{% set BuildPath_GA        = salt['pillar.get']('GA:BuildPath', '/usr')%}
{% set tomee_path          = salt['pillar.get']('tomee_path','NOT DEFINED') %}
{% set install_path        = salt['pillar.get'] ('install_path','NOT DEFINED') %}
{% set war_name            = salt['pillar.get'] ('war_name', 'NOT DEFINED') %}
{% set FCS_ROOT_PATH       = salt['pillar.get']('FCS_ROOT_PATH', '/usr/DassaultSystemes') %}
{% set FCS_GA_UI_FILE      = salt['file.join'](FCS_ROOT_PATH, 'fcs_GA_UserIntensions.xml') %}
{% set FCS_FP_UI_FILE      = salt ['file.join'] (FCS_ROOT_PATH, 'fcs_FP_UserIntensions.xml')%}

###############Install FCS GA ######################################
configure-fcs-folder:
  file.directory:
    - name: {{ FCS_ROOT_PATH }}

Install-FCS:
  file.managed:
    - source: salt://files/fcs_GA_UserIntensions.xml
    - name: {{ FCS_GA_UI_FILE }}
    - template: jinja
    - context:
      tomee_path: {{ tomee_path }}
      install_path: {{ install_path }}
      war_name: {{ war_name }}
    - failhard: True
    - require:
      - configure-fcs-folder
  
GA_Install_command:
  cmd.run:
    - name: {{ BuildPath_GA }}/FileCollaborationServer.Linux64/1/StartTUI.sh --silent {{ FCS_GA_UI_FILE }}
    - require:
      - file: {{ FCS_GA_UI_FILE }}
    - failhard: True
  

########### Install FCS FD ####################################

Install-FP-FCS:
  file.managed:
    - source: salt://files/fcs_FP_UserIntensions.xml
    - name: {{FCS_FP_UI_FILE}}
    - template: jinja
    - context:
      install_path: {{ install_path }}
    - failhard: True
    - require:
      - Install-FCS

FP_Install_command:
  cmd.run:
    - name: {{ BuildPath_GA }}/FileCollaborationServer-V62017X-HF3.Linux64/1/StartTUI.sh --silent {{ FCS_FP_UI_FILE }}
    - require:
      - file: {{ FCS_FP_UI_FILE }}
    - failhard: True