user_test1:
  user.present:
    - name: testu
    - fullname: test1 user
    - shell: /bin/bash
    - home: /home/test1

test1_user_key:
  ssh_auth.present:
    - name: test1
    - user: test1
    - source: salt://users/keys/test1.pub