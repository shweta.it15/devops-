#File-jinja.sls
# make file based on os version 
make_a_file_from_jinja:
  file.managed:
    {% if 7 in grains['osrelease_info'] %}
    - name: /tmp/salt-test/testfile_for7
    {% elif 1 in grains ['osrelease_info'] %}
    - name: /tmp/salt-test/testfile_for1
    {% else %}
    - name: /tmp/salt-test/we_don't_know_file
    {% endif %}
    - makedirs: true
  cmd.run:
    - name: echo "Create a file based on os release level"
    - onchanges:
      - file: make_a_file_from_jinja

