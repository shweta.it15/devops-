myapp:
  git.latest:
    - name: https://github.com/saltstack/pepper.git
    - rev: master
    - target: /var/www/myapp

notify_of_fail:
  event.send:
    - name: myco/myapp/fail_deploy
    - onfail: 
      - git: myapp
  