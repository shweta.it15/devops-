#Manage Apache
  {% if salt.grains.get('os_family') == 'RedHat' %}
  {% set apache_pkg = 'httpd' %}
  {% elif salt.grains.get('os_family') == 'Debian' %}
  {% set apache_pkg = 'apache2' %}
  {% endif %}

install_apache:
  pkg.installed:
    - name: {{  apache_pkg }}

start_apache:
  service.running:
    - name: {{ apache_pkg }} 
    - enable: true   
    #- require:
    #  - pkg: install_apache
    - watch:
      - file: some_config 

some_config:
  file.managed:
    - name: /tmp/foo
    - contents:
      - 'test file' 
      - 'added second line'
  