makeafile:
  file.managed:
    - name: /tmp/test/testfile.txt
    - makedirs: True
    - contents:
      - I made a file with this string on the first line.
      
add_line:  
  file.append:
    - name: /tmp/test/testfile.txt
    - text:
      - adding second line in the same file

notify_on_Fail:
  event.send:
    - name: file got created 
    - onfail: 
      - file: makeafile