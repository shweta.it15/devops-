user_test:
  user.present:
    - name: testu
    - fullname: testu user
    - shell: /bin/bash
    - home: /home/testu

testu_user_key:
  ssh_auth.present:
    - name: testu
    - user: testu
    - source: salt://users/keys/testu.pub