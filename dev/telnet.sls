install telnet:
  pkg:
    - installed

/tmp/vimrtestfile:
  file.managed:
    - source: salt://tmp/testfile
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: telnet
      