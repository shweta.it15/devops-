{% set version_long = salt['pillar.get'] ('java:version:long', 'jdk1.8.0_151' ) %}
{% set version_short = salt['pillar.get'] ('java.version:short', '8u151' ) %}

{% if salt['pkg.version'] ('jdk1.8') == ''%}
install java:
  pkg.installed:
    - sources:
      - jdk1.8: /tmp/jdk-8u151-linux-x64.rpm
{% endif %}

jdk_symlink:
  file.symlink:
      - name: /usr/java/latest
      - target: /usr/java/{{ version_long}}
      - makedirs: true

jdk_java_home_env:
  environ.setenv:
    - name: JAVA_HOME
    - value: /usr/java/latest

  


  