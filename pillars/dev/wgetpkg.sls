wgetpkg:
  {% if grains['os_family'] == 'RedHat' %}
  wget: wget
  {% elif grains['os_family'] == 'Debian' %}
  wget: wgets
  {% elif grains['os'] == 'Arch'  %}
  wget: wget
  {% endif %}

  